echo off
echo *****Início da Carga da tabela TABL120 MRD-iMRD *****
echo *****              %date% - %time%             *****

sqlldr userid=imrd_f/imrd@catalog direct=TRUE ERRORS=20000000 control=files_carga/TABL120.ctl log=files_carga/TABL120.log

echo **************  Fim da carga da tabela TABL120 - MRD-iMRD *************************
echo **************           %date% - %time%           *************************