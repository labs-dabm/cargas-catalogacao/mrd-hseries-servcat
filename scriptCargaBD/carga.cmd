echo off
echo *****Início da Carga das tabelas do MRD-iMRD *****
echo *****              %date% - %time%             *****

echo ***** Carregando a tabela EN_MRD0107 -- %date% - %time% ***** > files_carga/MRD0107.log
sqlplus imrd_f/imrd@catalog @files_carga/MRD0107.SQL /nolog  >>  files_carga/MRD0107.log

echo *****Carregando a tabela EN_MRD0300 -- %date% - %time% ******** > files_carga/MRD0300.log
sqlplus imrd_f/imrd@catalog @files_carga/MRD0300.SQL /nolog  >>  files_carga/MRD0300.log

echo *****Carregando a tabela EN_MRD0500 -- %date% - %time% ******** > files_carga/MRD0500.log
sqlplus imrd_f/imrd@catalog @files_carga/MRD0500.SQL /nolog  >>  files_carga/MRD0500.log

echo *****Carregando a tabela EN_MRD06P1 -- %date% - %time% ******** > files_carga/MRD06P1.log
sqlplus imrd_f/imrd@catalog @files_carga/MRD06P1.SQL /nolog  >>  files_carga/MRD06P1.log

echo *****Carregando a tabela EN_MRD06P2 -- %date% - %time% ******** > files_carga/MRD06P2.log
sqlplus imrd_f/imrd@catalog @files_carga/MRD06P2.SQL /nolog  >>  files_carga/MRD06P2.log

echo ***** Carregando a tabela EN_TABL076 -- %date% - %time% ***** > files_carga/TABL076.log
sqlplus imrd_f/imrd@catalog @files_carga/TABL076.SQL /nolog  >>  files_carga/TABL076.log

echo ***** Carregando a tabela EN_TABL098 -- %date% - %time% ***** > files_carga/TABL098.log
sqlplus imrd_f/imrd@catalog @files_carga/TABL098.SQL /nolog  >>  files_carga/TABL098.log

echo ***** Carregando a tabela EN_TABL099 -- %date% - %time% ***** > files_carga/TABL099.log
sqlplus imrd_f/imrd@catalog @files_carga/TABL099.SQL /nolog  >>  files_carga/TABL099.log

echo ***** Carregando a tabela EN_TABL316 -- %date% - %time% ***** > files_carga/TABL316.log
sqlplus imrd_f/imrd@catalog @files_carga/TABL316.SQL /nolog  >>  files_carga/TABL316.log

echo **************  Fim da carga das tabelas MRD-iMRD *************************
echo **************           %date% - %time%           *************************