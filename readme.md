## PROCEDIMENTOS PARA ATUALIZAÇÃO DO MRD E H-SERIES NO SERVCAT

![Fluxo da Atualização do MRD/H-SERIES no SERVCAT](wiki/img/Fluxo_MRD_H-Series.jpg)

### I. DOWNLOADS DOS ARQUIVOS PARA IMPORTAÇÃO:

##### 1º) No síto [**DefenseLogistics Agency** (DLA)](http://www.dla.mil/HQ/InformationOperations/Offers/Services/FIC/CatalogToolsTables.aspx), baixar os arquivos do MRD: 

 - Arquivos do [**MRD Traditional**](http://www.dla.mil/HQ/InformationOperations/Offers/Services/FIC/CatalogToolsTables.aspx#MRDT):

Contém todo o Diretório de Requisitos Mestres (VersãoTradicional). O conteúdo do arquivo zipado:
    MRD0107.TXT;
    MRD0300.TXT;
    MRD0500.TXT;
    MRD06P1.TXT;
    MRD06P2.TXT;
    MRD-RL.TXT – Layouts dos arquivos acima – não usado para importação. 

-  Arquivo **[Table098 NAMES_BY_TYPE](http://www.dla.mil/Portals/104/Documents/InformationOperations/LogisticsInformationServices/CatalogTools%20Tables/Tabl098.zip)**:

Contém todos os INCs, nomes coloquiais e nomes básicos. Oconteúdo do arquivo zipado:
    TABL098.txt;
    TABL098-RL.TXT –Layout do arquivo – não usado para importação.

- Arquivo [**Table076 FSC_NOTE_DAT_GROUP**](http://www.dla.mil/Portals/104/Documents/InformationOperations/LogisticsInformationServices/CatalogTools%20Tables/Tabl076.zip):

Contém todos os FSC ativos. O conteúdo do arquivo zipado:

    TABL076.txt;
    TABL076-RL.TXT –Layout do arquivo – não usado para importação.

- Arquivo [**Table099 NM_CD_FSC_XREF**](http://www.dla.mil/Portals/104/Documents/InformationOperations/LogisticsInformationServices/CatalogTools%20Tables/Tabl099.zip):

Identifica a Classe Federal de Abastecimento (FSC)permitida para um INC específico. O conteúdo do arquivo zipado:

    TABL099.txt;
    TABL099-RL.TXT –Layout do arquivo – não usado para importação.

- Arquivo [**Table120 EDIT_GUIDE_1**](http://www.dla.mil/Portals/104/Documents/InformationOperations/LogisticsInformationServices/CatalogTools%20Tables/Tabl120.zip):

Contém edições para os Requisitos de Código de Nome do Item(LGD). O conteúdo do arquivo zipado:

    TABL120.txt;
    TABL120-RL.TXT –Layout do arquivo – não usado para importação.

- Arquivo [**Table316 FSG_NOTE_DAT_GROUP**](http://www.dla.mil/Portals/104/Documents/InformationOperations/LogisticsInformationServices/CatalogTools%20Tables/TABL316.zip?ver=2018-01-18-144419-453):

Contém todos os FSG ativos (Grupos). O conteúdo do arquivozipado:

    TABL316.TXT;
    TABL316-RL.TXT – Layout doarquivo – não usado para importação.

###  II. PROCESSO DE PREPARAÇÃO DOS ARQUIVOS PARA IMPORTAÇÃO:

##### 1º) Extrair todos os arquivos para um mesmo diretório: 

![](wiki/img/files.png)

##### 2º) Obter a ferramenta de importação **MrdTools_EN** no repositório **XXXXXXXXXX**

- XXXXXX


##### 3º) Seguir os seguintes procedimentos:

  - Instalar o Java Runtime Environment (JRE), versão 1.5 ou superior no computador (caso ainda não tenha sido instalado);
  - Copiar para a mesma pasta de descompactação dos arquivos à ferramenta de conversão de formatos “MrdTools_EN” (Foi feita uma adaptação da ferramenta para atender a necessidade do SERVCAT).


``` bash
# java –jar MrdTools.jar –opcao arquivo-origem.TXT arquivo-destino.SQL

# onde opção pode ser:
# -d107 – Quando arquivo-origem for o MRDT0107.TXT;
# -d300 – Quando arquivo-origem for o MRD0300.TXT;
# -d500 – Quando arquivo-origem for o MRD0500.TXT;
# -d6p1 – Quando arquivo-origem for o MRD06P1.TXT;
# -d6pII – Quando arquivo-origem for o MRD06P2.TXT;
# -d099 – Quando arquivo-origem for o TABL099.TXT;
# -d120 – Quando arquivo-origem for o TABL120.TXT;
# -dh2g – Quando arquivo-origem for o TABL316.TXT;
# -dh2C – Quando arquivo-origem for o TABL076.TXT; e
# -dh6 – Quando arquivo-origem for o TABL098.TXT.

# Exemplos
java -jar MrdTools-EN.jar -d6p1 MRD06P1.txt MRD06P1.SQL
java -jar MrdTools-EN.jar -d6pII MRD06P2.txt MRD06P2.SQL
java -jar MrdTools-EN.jar -d300 MRD0300.txt MRD0300.SQL
java -jar MrdTools-EN.jar -d500 MRD0500.txt MRD0500.SQL 
java -jar MrdTools-EN.jar -dh2C TABL076.txt TABL076.SQL
java -jar MrdTools-EN.jar -dh6 TABL098.txt TABL098.SQL  
java -jar MrdTools-EN.jar -d099 TABL099.txt TABL099.SQL  
java -jar MrdTools-EN.jar -d120 TABL120P1.txt TABL120P1.SQL
java -jar MrdTools-EN.jar -d120 TABL120P2.txt TABL120P2.SQL
java -jar MrdTools-EN.jar -dh2g TABL316.TXT TABL316.SQL
```

Após a execução dos comandos acima, serão gerados os SQLs prontos para serem importados na Base de Dados do Oracle. Entretanto antes de importa-los para a BD faz-se necessários **“Dropar”** e recriar as tabelas a cada nova atualização, basta rodar os seguintes scripts para a execução dessa etapa.

Arquivo: **create_EN_MRD_098_H6.sql**

``` sql
DROP TABLE IMRD_F.EN_MRD098_H6 CASCADE CONSTRAINTS;

--criando a tabela com a PK ID
CREATE TABLE IMRD_F.EN_MRD098_H6
(
    NM_CD_2303          VARCHAR2(2048 BYTE),
    APPLB_KEY_CD_0103   VARCHAR2(2048 BYTE),
    DT_ESTB_CANC_0308   VARCHAR2(2048 BYTE),
    STA_CD_NM_FSC_0320  VARCHAR2(2048 BYTE),
    SHRT_NM_2301        VARCHAR2(2048 BYTE),
    NM_PREFIX_2516      VARCHAR2(2048 BYTE),
    NM_ROOT_RMDR_2517   VARCHAR2(2048 BYTE),
    FSC_COND_CD_2607    VARCHAR2(2048 BYTE),
    ITM_NM_TYP_CD_3308  VARCHAR2(2048 BYTE),
    FIIG_4065           VARCHAR2(2048 BYTE),
    CONCEPT_NBR_4488    VARCHAR2(2048 BYTE),
    ITM_NM_DEF_5015     VARCHAR2(2048 BYTE),
    ID                  NUMBER(19),
    PRIMARY KEY(ID)
)
TABLESPACE IMRD_FTBL
RESULT_CACHE (MODE DEFAULT)
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
            FLASH_CACHE      DEFAULT
            CELL_FLASH_CACHE DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;

--criando os sinonimos publicos
CREATE OR REPLACE PUBLIC SYNONYM EN_MRD098_H6 FOR IMRD_F.EN_MRD098_H6;

--dropando a sequence
DROP SEQUENCE IMRD_F.SQ_EN_TABL098;

--criando a sequence
CREATE SEQUENCE IMRD_F.SQ_EN_TABL098
  START WITH 1
  MAXVALUE 9999999999999999999999999999
  MINVALUE 0
  NOCYCLE
  NOCACHE
  NOORDER;

--criando indices
CREATE UNIQUE INDEX IDX_EN_MRD098_H6_INC ON IMRD_F.EN_MRD098_H6(NM_CD_2303) TABLESPACE IMRD_FTBL; 
```
Arquivo: **create_EN_MRD_H6.sql**
``` sql
DROP TABLE IMRD_F.EN_MRD_H6 CASCADE CONSTRAINTS;

--criando a tabela com a PK ID
CREATE TABLE IMRD_F.EN_MRD_H6
(
    NM_CD_2303          VARCHAR2(2048 BYTE),
    APPLB_KEY_CD_0103   VARCHAR2(2048 BYTE),
    DT_ESTB_CANC_0308   VARCHAR2(2048 BYTE),
    STA_CD_NM_FSC_0320  VARCHAR2(2048 BYTE),
    SHRT_NM_2301        VARCHAR2(2048 BYTE),
    NM_PREFIX_2516      VARCHAR2(2048 BYTE),
    NM_ROOT_RMDR_2517   VARCHAR2(2048 BYTE),
    FSC_COND_CD_2607    VARCHAR2(2048 BYTE),
    ITM_NM_TYP_CD_3308  VARCHAR2(2048 BYTE),
    FIIG_4065           VARCHAR2(2048 BYTE),
    CONCEPT_NBR_4488    VARCHAR2(2048 BYTE),
    ITM_NM_DEF_5015     VARCHAR2(2048 BYTE),
    NM_FLD_2304         VARCHAR2(2048 BYTE), --concat SHRT_NM_2301 || NM_ROOT_RMDR_2517
    ID                  NUMBER(19),
    PRIMARY KEY(ID)
)
TABLESPACE IMRD_FTBL
RESULT_CACHE (MODE DEFAULT)
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
            FLASH_CACHE      DEFAULT
            CELL_FLASH_CACHE DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;

--criando os sinonimos publicos
CREATE OR REPLACE PUBLIC SYNONYM EN_MRD_H6 FOR IMRD_F.EN_MRD_H6;

--criando indices
CREATE UNIQUE INDEX IDX_EN_MRD_H6_INC ON IMRD_F.EN_MRD_H6(NM_CD_2303) TABLESPACE IMRD_FTBL; 
```

Arquivo: **create_EN_MRD06P1.sql**
``` sql
-------------
DROP TABLE IMRD_F.EN_MRD06P1 CASCADE CONSTRAINTS;

--criando a tabela com a PK ID
CREATE TABLE IMRD_F.EN_MRD06P1
(
    INC_4080            VARCHAR2(2048 BYTE),
    FIIG_4065           VARCHAR2(2048 BYTE),
    MRC_3445            VARCHAR2(2048 BYTE),
    RPLY_TBL_DCOD_3845  VARCHAR2(2048 BYTE),
    ID                  NUMBER(19),
    PRIMARY KEY(ID)
)
TABLESPACE IMRD_FTBL
RESULT_CACHE (MODE DEFAULT)
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
            FLASH_CACHE      DEFAULT
            CELL_FLASH_CACHE DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;

--criando os sinonimos publicos
CREATE OR REPLACE PUBLIC SYNONYM EN_MRD06P1 FOR IMRD_F.EN_MRD06P1;

--dropando a sequence
DROP SEQUENCE IMRD_F.SQ_EN_TABL06P1;
--criando a sequence
CREATE SEQUENCE IMRD_F.SQ_EN_TABL06P1
  START WITH 1
  MAXVALUE 9999999999999999999999999999
  MINVALUE 0
  NOCYCLE
  NOCACHE
  NOORDER;

--criando indices
CREATE UNIQUE INDEX IDX_EN_MRD06P1_FIIG_INC_MRC ON IMRD_F.EN_MRD06P1(FIIG_4065, INC_4080, MRC_3445) TABLESPACE IMRD_FTBL;
```

Arquivo: **create_EN_MRD06P2.sql**
``` sql
-----------
DROP TABLE IMRD_F.EN_MRD06P2 CASCADE CONSTRAINTS;

--criando a tabela com a PK ID
CREATE TABLE IMRD_F.EN_MRD06P2
(
    RPLY_TBL_DCOD_3845     VARCHAR2(2048 BYTE),
    SEC_AD_IND_CD_9485     VARCHAR2(2048 BYTE),
    SAC_8990               VARCHAR2(2048 BYTE),
    DCOD_ISAC_RPY_2308     VARCHAR2(2048 BYTE),
    ID                     NUMBER(19),
    PRIMARY KEY(ID)
)
TABLESPACE IMRD_FTBL
RESULT_CACHE (MODE DEFAULT)
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
            FLASH_CACHE      DEFAULT
            CELL_FLASH_CACHE DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;

--criando os sinonimos publicos
CREATE OR REPLACE PUBLIC SYNONYM EN_MRD06P2 FOR IMRD_F.EN_MRD06P2;

--dropando a sequence
DROP SEQUENCE IMRD_F.SQ_EN_TABL06P2;
--criando a sequence
CREATE SEQUENCE IMRD_F.SQ_EN_TABL06P2
  START WITH 1
  MAXVALUE 9999999999999999999999999999
  MINVALUE 0
  NOCYCLE
  NOCACHE
  NOORDER;

--criando indices
CREATE UNIQUE INDEX IDX_EN_MRD06P2_RPLY_SAC ON IMRD_F.EN_MRD06P2(RPLY_TBL_DCOD_3845, SAC_8990) TABLESPACE IMRD_FTBL;
```

Arquivo: **create_EN_MRD076_H2_CLASSE.sql**
``` sql
--------
DROP TABLE IMRD_F.EN_MRD076_H2_CLASSE CASCADE CONSTRAINTS;

--criando a tabela com a PK ID
CREATE TABLE IMRD_F.EN_MRD076_H2_CLASSE
(
    FSG_3994              VARCHAR2(2048 BYTE),
    FSC_WI_FSG_3996       VARCHAR2(2048 BYTE),
    STA_CD_NM_FSC_0320    VARCHAR2(2048 BYTE),
    FSC_INCL_NAR_0375     VARCHAR2(2048 BYTE),
    FSC_EXCL_NAR_0376     VARCHAR2(2048 BYTE),
    FSC_NOTE_4962         VARCHAR2(2048 BYTE),
    FSC_TI_4970           VARCHAR2(2048 BYTE),
    FSC_DT_E_C_RC_5182    VARCHAR2(2048 BYTE),
    ID                    NUMBER(19),
    PRIMARY KEY(ID)
)
TABLESPACE IMRD_FTBL
RESULT_CACHE (MODE DEFAULT)
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
            FLASH_CACHE      DEFAULT
            CELL_FLASH_CACHE DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;

--criando os sinonimos publicos
CREATE OR REPLACE PUBLIC SYNONYM EN_MRD076_H2_CLASSE FOR IMRD_F.EN_MRD076_H2_CLASSE;

--dropando a sequence
DROP SEQUENCE IMRD_F.SQ_EN_TABL076;
--criando a sequence
CREATE SEQUENCE IMRD_F.SQ_EN_TABL076
  START WITH 1
  MAXVALUE 9999999999999999999999999999
  MINVALUE 0
  NOCYCLE
  NOCACHE
  NOORDER;

--criando indices
CREATE UNIQUE INDEX IDX_EN_MRD076_H2_FSG_FSC ON IMRD_F.EN_MRD076_H2_CLASSE(FSG_3994, FSC_WI_FSG_3996) TABLESPACE IMRD_FTBL;
```

Arquivo: **create_EN_MRD099_INC_CLASSE.sql**
``` sql
------
DROP TABLE IMRD_F.EN_MRD099_INC_CLASSE CASCADE CONSTRAINTS;

--criando a tabela com a PK ID
CREATE TABLE IMRD_F.EN_MRD099_INC_CLASSE
(
    NM_CD_2303          VARCHAR2(2048 BYTE),
    FSG_3994            VARCHAR2(2048 BYTE),
    FSC_WI_FSG_3996     VARCHAR2(2048 BYTE),
    DT_ESTB_CANC_0308   VARCHAR2(2048 BYTE),
    EFF_DT_2128         VARCHAR2(2048 BYTE),
    CL_ASST_MODH2_9554  VARCHAR2(2048 BYTE),
    ID                  NUMBER(19),
    PRIMARY KEY(ID)
)
TABLESPACE IMRD_FTBL
RESULT_CACHE (MODE DEFAULT)
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
            FLASH_CACHE      DEFAULT
            CELL_FLASH_CACHE DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;

--criando os sinonimos publicos
CREATE OR REPLACE PUBLIC SYNONYM EN_MRD099_INC_CLASSE FOR IMRD_F.EN_MRD099_INC_CLASSE;


--dropando a sequence
DROP SEQUENCE IMRD_F.SQ_EN_TABL099;
--criando a sequence
CREATE SEQUENCE IMRD_F.SQ_EN_TABL099
  START WITH 1
  MAXVALUE 9999999999999999999999999999
  MINVALUE 0
  NOCYCLE
  NOCACHE
  NOORDER;

--criando indices
CREATE UNIQUE INDEX IDX_EN_MRD099_INC_FSG_FSC ON IMRD_F.EN_MRD099_INC_CLASSE(NM_CD_2303, FSG_3994, FSC_WI_FSG_3996) TABLESPACE IMRD_FTBL;
```

Arquivo: **create_EN_MRD0107.sql**
``` sql
---------------------
DROP TABLE IMRD_F.EN_MRD0107 CASCADE CONSTRAINTS;

--criando a tabela com a PK ID
CREATE TABLE IMRD_F.EN_MRD0107
(
    KEYWRD_GRP_CD_2034            VARCHAR2(2048 BYTE),
    KEYWRD_MOD_ST_2033            VARCHAR2(2048 BYTE),
    MODE_CD_4735                  VARCHAR2(2048 BYTE),
    MRC_3445                      VARCHAR2(2048 BYTE),
    MRC_TBL_CT_0848               VARCHAR2(2048 BYTE),
    MRC_USG_DESI_0847             VARCHAR2(2048 BYTE),
    MRD_STATUS_CD_0816            VARCHAR2(2048 BYTE),
    PRT_SKLTN_CD_0368             VARCHAR2(2048 BYTE),
    RQMT_RPY_INST_2648            VARCHAR2(2048 BYTE),
    RQMT_STMT_3614                VARCHAR2(2048 BYTE),
    RQMT_STMT_DEF_5027            VARCHAR2(2048 BYTE),
    TAM_RESP_COD_TAB_0365_E_8254  VARCHAR2(2048 BYTE),
    ID                            NUMBER(19),
    PRIMARY KEY(ID)
)
TABLESPACE IMRD_FTBL
RESULT_CACHE (MODE DEFAULT)
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
            FLASH_CACHE      DEFAULT
            CELL_FLASH_CACHE DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;

--criando os sinonimos publicos
CREATE OR REPLACE PUBLIC SYNONYM EN_MRD0107 FOR IMRD_F.EN_MRD0107;
--dropando a sequence
DROP SEQUENCE IMRD_F.SQ_EN_TABL0107;
--criando a sequence
CREATE SEQUENCE IMRD_F.SQ_EN_TABL0107
  START WITH 1
  MAXVALUE 9999999999999999999999999999
  MINVALUE 0
  NOCYCLE
  NOCACHE
  NOORDER;
  
--criando indices
CREATE UNIQUE INDEX IDX_EN_MRD0107_INC ON IMRD_F.EN_MRD0107(MRC_3445) TABLESPACE IMRD_FTBL;  
```

Arquivo: **create_EN_MRD120.sql**
``` sql
DROP TABLE IMRD_F.EN_MRD120 CASCADE CONSTRAINTS;
--criando a tabela com a PK ID
CREATE TABLE IMRD_F.EN_MRD120
(
    FIIG_4065           VARCHAR2(2048 BYTE),
    INC_4080            VARCHAR2(2048 BYTE),
    MRC_3445            VARCHAR2(2048 BYTE),
    SAC_CDNG_IND_0115   VARCHAR2(2048 BYTE),
    DIML_MRC_IND_0326   VARCHAR2(2048 BYTE),
    PRPTN_EDIT_CD_0762  VARCHAR2(2048 BYTE),
    ISAC_IND_CD_0826    VARCHAR2(2048 BYTE),
    SCR_KEY_CD_1047     VARCHAR2(2048 BYTE),
    RPLY_TBL_DCOD_3845  VARCHAR2(2048 BYTE),
    TAIL_SEQ_NO_4403    VARCHAR2(2048 BYTE),
    FIIG_SEQ_NO_4404    VARCHAR2(2048 BYTE),
    MRC_IND_CD_8450     VARCHAR2(2048 BYTE),
    ID                  NUMBER(19),
    --CAR_ITEM_ID         NUMBER(38), -- vrf se tem necessiade no servcat?
    --ARTIGO_ID           NUMBER(38)  -- vrf se tem necessiade no servcat?
    PRIMARY KEY(ID)
)
TABLESPACE IMRD_FTBL
RESULT_CACHE (MODE DEFAULT)
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
            FLASH_CACHE      DEFAULT
            CELL_FLASH_CACHE DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;

--criando os sinonimos publicos
CREATE OR REPLACE PUBLIC SYNONYM EN_MRD120 FOR IMRD_F.EN_MRD120;

--dropando a sequence
DROP SEQUENCE IMRD_F.SQ_EN_TABL0120;
--criando a sequence
CREATE SEQUENCE IMRD_F.SQ_EN_TABL0120
  START WITH 1
  MAXVALUE 9999999999999999999999999999
  MINVALUE 0
  NOCYCLE
  NOCACHE
  NOORDER;

--criando indices
CREATE UNIQUE  INDEX IDX_EN_MRD120_MRC_INC ON IMRD_F.EN_MRD120(MRC_3445, INC_4080) TABLESPACE IMRD_FTBL;
CREATE UNIQUE  INDEX IDX_EN_MRD120_MRC_INC_FIIG ON IMRD_F.EN_MRD120(MRC_3445, INC_4080, FIIG_4065) TABLESPACE IMRD_FTBL;
```

Arquivo: **create_EN_MRD0300.sql**
``` sql
-----
DROP TABLE IMRD_F.EN_MRD0300 CASCADE CONSTRAINTS;

--criando a tabela com a PK ID
CREATE TABLE IMRD_F.EN_MRD0300
(
    RPLY_TBL_MRD_8254     VARCHAR2(2048 BYTE),
    CDD_RPLY_3465         VARCHAR2(2048 BYTE),
    RPLY_STAT_IND_2498    VARCHAR2(2048 BYTE),
    DCOD_RPLY_ST_3864     VARCHAR2(2048 BYTE),
    ID                    NUMBER(19),
    PRIMARY KEY(ID)
)
TABLESPACE IMRD_FTBL
RESULT_CACHE (MODE DEFAULT)
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
            FLASH_CACHE      DEFAULT
            CELL_FLASH_CACHE DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;

--criando os sinonimos publicos
CREATE OR REPLACE PUBLIC SYNONYM EN_MRD0300 FOR IMRD_F.EN_MRD0300;

--dropando a sequence
DROP SEQUENCE IMRD_F.SQ_EN_TABL300;
--criando a sequence
CREATE SEQUENCE IMRD_F.SQ_EN_TABL300
  START WITH 1
  MAXVALUE 9999999999999999999999999999
  MINVALUE 0
  NOCYCLE
  NOCACHE
  NOORDER;

--criando indices
CREATE UNIQUE INDEX IDX_EN_MRD0300_RPLY_CDD ON IMRD_F.EN_MRD0300(RPLY_TBL_MRD_8254, CDD_RPLY_3465) TABLESPACE IMRD_FTBL;
```

Arquivo: **create_EN_MRD316_H2_GRUPO.sql**
``` sql
DROP TABLE IMRD_F.EN_MRD316_H2_GRUPO CASCADE CONSTRAINTS;
--criando a tabela com a PK ID
CREATE TABLE IMRD_F.EN_MRD316_H2_GRUPO
(
    FSG_3994            VARCHAR2(2048 BYTE),
    STA_CD_NM_FSC_0320  VARCHAR2(2048 BYTE),
    FSG_NOTE_4965       VARCHAR2(2048 BYTE),
    FSG_TI_4972         VARCHAR2(2048 BYTE),
    FSG_DT_E_C_RC_5183  VARCHAR2(2048 BYTE),
    ID                  NUMBER(19)                NOT NULL,
    PRIMARY KEY(ID)
)
TABLESPACE IMRD_FTBL
RESULT_CACHE (MODE DEFAULT)
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
            FLASH_CACHE      DEFAULT
            CELL_FLASH_CACHE DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;

--criando os sinonimos publicos
CREATE OR REPLACE PUBLIC SYNONYM EN_MRD316_H2_GRUPO FOR IMRD_F.EN_MRD316_H2_GRUPO;

--dropando a sequence
DROP SEQUENCE IMRD_F.SQ_EN_TABL316;
--criando a sequence
CREATE SEQUENCE IMRD_F.SQ_EN_TABL316
  START WITH 1
  MAXVALUE 9999999999999999999999999999
  MINVALUE 0
  NOCYCLE
  NOCACHE
  NOORDER;

--criando indices
CREATE UNIQUE  INDEX IDX_EN_MRD316_H2_GRUPO_FSG ON IMRD_F.EN_MRD316_H2_GRUPO(FSG_3994) TABLESPACE IMRD_FTBL;
```

Arquivo: **create_EN_MRD0500.sql**
``` sql
-----
DROP TABLE IMRD_F.EN_MRD0500 CASCADE CONSTRAINTS;

--criando a tabela com a PK ID
CREATE TABLE IMRD_F.EN_MRD0500
(
    INC_4080               VARCHAR2(5 BYTE),
    FIIG_4065              VARCHAR2(2048 BYTE),
    MRC_3445               VARCHAR2(2048 BYTE),
    PHYS_DWG_ID_5037       VARCHAR2(8 BYTE),
    DCOD_STYL_RPY_2309     VARCHAR2(2048 BYTE),
    STYL_NBR_FIIG_0768     VARCHAR2(2048 BYTE),
    ID                     NUMBER(19),
    PRIMARY KEY(ID)
)
TABLESPACE IMRD_FTBL
RESULT_CACHE (MODE DEFAULT)
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
            FLASH_CACHE      DEFAULT
            CELL_FLASH_CACHE DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;

--criando os sinonimos publicos
CREATE OR REPLACE PUBLIC SYNONYM EN_MRD0500 FOR IMRD_F.EN_MRD0500;

--dropando a sequence
DROP SEQUENCE IMRD_F.SQ_EN_TABL500;
--criando a sequence
CREATE SEQUENCE IMRD_F.SQ_EN_TABL500
  START WITH 1
  MAXVALUE 9999999999999999999999999999
  MINVALUE 0
  NOCYCLE
  NOCACHE
  NOORDER;

--criando indices
CREATE UNIQUE INDEX IDX_MRD0500_INC_FIIG_MRC_STYL ON IMRD_F.EN_MRD0500(INC_4080, FIIG_4065, MRC_3445, STYL_NBR_FIIG_0768) TABLESPACE IMRD_FTBL;
```

### II. PROCESSO DE IMPORTAÇÃO:

##### 1º) Após a execução dos scripts de **criação das tabelas** e gerado os **SQLs**, execute os passos abaixo:

​	Para executar os scripts você terá que ter a seguinte estrutura de pastas:

![Estrutura do diretório para executar o script](wiki/img/EstruturaScriptCargaBD.PNG)

- Executar o processo de carga, pelo arquivo criado de **carga.cmd**;

```powershell
echo off
echo *****Início da Carga das tabelas do MRD-iMRD *****
echo *****              %date% - %time%             *****

echo ***** Carregando a tabela EN_MRD0107 -- %date% - %time% ***** > files_carga/MRD0107.log
sqlplus imrd_f/imrd@catalog @files_carga/MRD0107.SQL /nolog  >>  files_carga/MRD0107.log

echo *****Carregando a tabela EN_MRD0300 -- %date% - %time% ******** > files_carga/MRD0300.log
sqlplus imrd_f/imrd@catalog @files_carga/MRD0300.SQL /nolog  >>  files_carga/MRD0300.log

echo *****Carregando a tabela EN_MRD0500 -- %date% - %time% ******** > files_carga/MRD0500.log
sqlplus imrd_f/imrd@catalog @files_carga/MRD0500.SQL /nolog  >>  files_carga/MRD0500.log

echo *****Carregando a tabela EN_MRD06P1 -- %date% - %time% ******** > files_carga/MRD06P1.log
sqlplus imrd_f/imrd@catalog @files_carga/MRD06P1.SQL /nolog  >>  files_carga/MRD06P1.log

echo *****Carregando a tabela EN_MRD06P2 -- %date% - %time% ******** > files_carga/MRD06P2.log
sqlplus imrd_f/imrd@catalog @files_carga/MRD06P2.SQL /nolog  >>  files_carga/MRD06P2.log

echo ***** Carregando a tabela EN_TABL076 -- %date% - %time% ***** > files_carga/TABL076.log
sqlplus imrd_f/imrd@catalog @files_carga/TABL076.SQL /nolog  >>  files_carga/TABL076.log

echo ***** Carregando a tabela EN_TABL098 -- %date% - %time% ***** > files_carga/TABL098.log
sqlplus imrd_f/imrd@catalog @files_carga/TABL098.SQL /nolog  >>  files_carga/TABL098.log

echo ***** Carregando a tabela EN_TABL099 -- %date% - %time% ***** > files_carga/TABL099.log
sqlplus imrd_f/imrd@catalog @files_carga/TABL099.SQL /nolog  >>  files_carga/TABL099.log

echo ***** Carregando a tabela EN_TABL316 -- %date% - %time% ***** > files_carga/TABL316.log
sqlplus imrd_f/imrd@catalog @files_carga/TABL316.SQL /nolog  >>  files_carga/TABL316.log

echo **************  Fim da carga das tabelas MRD-iMRD *************************
echo **************           %date% - %time%           *************************
```

- Executar o processo de carga, pelo arquivo criado de **carga-sqlloader.cmd**;

```powershell
echo off
echo *****Início da Carga da tabela TABL120 MRD-iMRD *****
echo *****              %date% - %time%             *****

sqlldr userid=imrd_f/imrd@catalog direct=TRUE ERRORS=20000000 control=files_carga/TABL120.ctl log=files_carga/TABL120.log

echo **************  Fim da carga da tabela TABL120 - MRD-iMRD *************************
echo **************           %date% - %time%           *************************
```



##### 2º) Copiar o conteúdo da tabela: `EN_MRD098_H6` para `EN_MRD_H6`

​	Para executar os scripts você terá que ter a seguinte estrutura de pastas:

``` sql
-- Limpar a tabela
TRUNCATE TABLE EN_MRD_H6;
-- inserir itens da tabela EN_MRD098_H6 para EN_MRD_H6
-- concatenando as colunas SHRT_NM_2301||NM_ROOT_RMDR_2517
INSERT INTO EN_MRD_H6 (NM_CD_2303,
                       APPLB_KEY_CD_0103,
                       DT_ESTB_CANC_0308,
                       STA_CD_NM_FSC_0320,
                       SHRT_NM_2301,
                       NM_PREFIX_2516,
                       NM_ROOT_RMDR_2517,
                       FSC_COND_CD_2607,
                       ITM_NM_TYP_CD_3308,
                       FIIG_4065,
                       CONCEPT_NBR_4488,
                       ITM_NM_DEF_5015,
                       NM_FLD_2304,
                       ID)
   (SELECT NM_CD_2303,
           APPLB_KEY_CD_0103,
           DT_ESTB_CANC_0308,
           STA_CD_NM_FSC_0320,
           SHRT_NM_2301,
           NM_PREFIX_2516,
           NM_ROOT_RMDR_2517,
           FSC_COND_CD_2607,
           ITM_NM_TYP_CD_3308,
           FIIG_4065,
           CONCEPT_NBR_4488,
           ITM_NM_DEF_5015,
           SHRT_NM_2301||NM_ROOT_RMDR_2517
           ID
      FROM EN_MRD098_H6);
-- confirmar a atualizacao
COMMIT;
-- deletar a tabela EN_MRD098_H6
DROP TABLE EN_MRD098_H6 CASCADE CONSTRAINTS;
-- confirmar a exclusão
COMMIT;      
```

##### 3º) Normalizar a carga da Tabela `EN_MRD0500`.

Execute o comando abaixo após a conclusão da carga e dos passos acima.

``` sql
-- Deletar as ocorrências FIIG, MRC, STYL com mais de uma ocorrência
DELETE FROM EN_MRD0500
WHERE ID NOT IN(SELECT MIN(ID) FROM EN_MRD0500
GROUP BY FIIG_4065, MRC_3445, STYL_NBR_FIIG_0768);

COMMIT;

-- CONCATENAR ESTILO COM A RESPOSTA PARA FICAR NO PADRÃO EXPORTADO PARA A COA_FA
UPDATE EN_MRD0500
SET DCOD_STYL_RPY_2309=STYL_NBR_FIIG_0768||' '||DCOD_STYL_RPY_2309
WHERE SUBSTR(DCOD_STYL_RPY_2309,1,(LENGTH(STYL_NBR_FIIG_0768)) )<>
STYL_NBR_FIIG_0768;

COMMIT;
```

##### 4º) Atualizar a data da Manutenção do MRD e H-Series.

Execute o comando abaixo para atualziar a data da carga do grupo MRD e H-SERIES no BD.

```sql
INSERT INTO SEGV_DATA_ATUALIZACAO (DATA_DO_RAW, MANUTENCAO, PROGRAMA)
     VALUES ('19/02/18', 'F', 'IMRD_F.EN_MRD0107');
INSERT INTO SEGV_DATA_ATUALIZACAO (DATA_DO_RAW, MANUTENCAO, PROGRAMA)
     VALUES ('19/02/18', 'F', 'IMRD_F.EN_MRD0300');
INSERT INTO SEGV_DATA_ATUALIZACAO (DATA_DO_RAW, MANUTENCAO, PROGRAMA)
     VALUES ('19/02/18', 'F', 'IMRD_F.EN_MRD0500');   
INSERT INTO SEGV_DATA_ATUALIZACAO (DATA_DO_RAW, MANUTENCAO, PROGRAMA)
     VALUES ('19/02/18', 'F', 'IMRD_F.EN_MRD06P1');     
INSERT INTO SEGV_DATA_ATUALIZACAO (DATA_DO_RAW, MANUTENCAO, PROGRAMA)
     VALUES ('19/02/18', 'F', 'IMRD_F.EN_MRD06P2');     
INSERT INTO SEGV_DATA_ATUALIZACAO (DATA_DO_RAW, MANUTENCAO, PROGRAMA)
     VALUES ('19/02/18', 'F', 'IMRD_F.EN_MRD076_H2_CLASSE');     
INSERT INTO SEGV_DATA_ATUALIZACAO (DATA_DO_RAW, MANUTENCAO, PROGRAMA)
     VALUES ('19/02/18', 'F', 'IMRD_F.EN_MRD316_H2_GRUPO');     
INSERT INTO SEGV_DATA_ATUALIZACAO (DATA_DO_RAW, MANUTENCAO, PROGRAMA)
     VALUES ('19/02/18', 'F', 'IMRD_F.EN_MRD_H6');     
INSERT INTO SEGV_DATA_ATUALIZACAO (DATA_DO_RAW, MANUTENCAO, PROGRAMA)
     VALUES ('19/02/18', 'F', 'IMRD_F.EN_MRD099_INC_CLASSE');     
INSERT INTO SEGV_DATA_ATUALIZACAO (DATA_DO_RAW, MANUTENCAO, PROGRAMA)
     VALUES ('19/02/18', 'F', 'IMRD_F.EN_MRD120');     
INSERT INTO SEGV_DATA_ATUALIZACAO (DATA_DO_RAW, MANUTENCAO, PROGRAMA)
     VALUES ('19/02/18', 'F', 'IMRD_F.EN_MRD316_H2_GRUPO');          

COMMIT; 
```

