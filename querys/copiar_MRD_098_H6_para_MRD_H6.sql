TRUNCATE TABLE EN_MRD_H6;

INSERT INTO EN_MRD_H6 (NM_CD_2303,
                       APPLB_KEY_CD_0103,
                       DT_ESTB_CANC_0308,
                       STA_CD_NM_FSC_0320,
                       SHRT_NM_2301,
                       NM_PREFIX_2516,
                       NM_ROOT_RMDR_2517,
                       FSC_COND_CD_2607,
                       ITM_NM_TYP_CD_3308,
                       FIIG_4065,
                       CONCEPT_NBR_4488,
                       ITM_NM_DEF_5015,
                       NM_FLD_2304,
                       ID)
   (SELECT NM_CD_2303,
           APPLB_KEY_CD_0103,
           DT_ESTB_CANC_0308,
           STA_CD_NM_FSC_0320,
           SHRT_NM_2301,
           NM_PREFIX_2516,
           NM_ROOT_RMDR_2517,
           FSC_COND_CD_2607,
           ITM_NM_TYP_CD_3308,
           FIIG_4065,
           CONCEPT_NBR_4488,
           ITM_NM_DEF_5015,
           SHRT_NM_2301||H6.NM_ROOT_RMDR_2517
           ID
      FROM EN_MRD098_H6);
      
COMMIT; 