------
DROP TABLE IMRD_F.EN_MRD099_INC_CLASSE CASCADE CONSTRAINTS;

--criando a tabela com a PK ID
CREATE TABLE IMRD_F.EN_MRD099_INC_CLASSE
(
    NM_CD_2303          VARCHAR2(2048 BYTE),
    FSG_3994            VARCHAR2(2048 BYTE),
    FSC_WI_FSG_3996     VARCHAR2(2048 BYTE),
    DT_ESTB_CANC_0308   VARCHAR2(2048 BYTE),
    EFF_DT_2128         VARCHAR2(2048 BYTE),
    CL_ASST_MODH2_9554  VARCHAR2(2048 BYTE),
    ID                  NUMBER(19),
    PRIMARY KEY(ID)
)
TABLESPACE IMRD_FTBL
RESULT_CACHE (MODE DEFAULT)
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
            FLASH_CACHE      DEFAULT
            CELL_FLASH_CACHE DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;

--criando os sinonimos publicos
CREATE OR REPLACE PUBLIC SYNONYM EN_MRD099_INC_CLASSE FOR IMRD_F.EN_MRD099_INC_CLASSE;


--dropando a sequence
DROP SEQUENCE IMRD_F.SQ_EN_TABL099;
--criando a sequence
CREATE SEQUENCE IMRD_F.SQ_EN_TABL099
  START WITH 1
  MAXVALUE 9999999999999999999999999999
  MINVALUE 0
  NOCYCLE
  NOCACHE
  NOORDER;

--criando indices
CREATE UNIQUE INDEX IDX_EN_MRD099_INC_FSG_FSC ON IMRD_F.EN_MRD099_INC_CLASSE(NM_CD_2303, FSG_3994, FSC_WI_FSG_3996) TABLESPACE IMRD_FTBL;
