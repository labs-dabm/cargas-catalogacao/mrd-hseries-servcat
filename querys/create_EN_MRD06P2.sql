-----------
DROP TABLE IMRD_F.EN_MRD06P2 CASCADE CONSTRAINTS;

--criando a tabela com a PK ID
CREATE TABLE IMRD_F.EN_MRD06P2
(
    RPLY_TBL_DCOD_3845     VARCHAR2(2048 BYTE),
    SEC_AD_IND_CD_9485     VARCHAR2(2048 BYTE),
    SAC_8990               VARCHAR2(2048 BYTE),
    DCOD_ISAC_RPY_2308     VARCHAR2(2048 BYTE),
    ID                     NUMBER(19),
    PRIMARY KEY(ID)
)
TABLESPACE IMRD_FTBL
RESULT_CACHE (MODE DEFAULT)
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
            FLASH_CACHE      DEFAULT
            CELL_FLASH_CACHE DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;

--criando os sinonimos publicos
CREATE OR REPLACE PUBLIC SYNONYM EN_MRD06P2 FOR IMRD_F.EN_MRD06P2;

--dropando a sequence
DROP SEQUENCE IMRD_F.SQ_EN_TABL06P2;
--criando a sequence
CREATE SEQUENCE IMRD_F.SQ_EN_TABL06P2
  START WITH 1
  MAXVALUE 9999999999999999999999999999
  MINVALUE 0
  NOCYCLE
  NOCACHE
  NOORDER;

--criando indices
CREATE UNIQUE INDEX IDX_EN_MRD06P2_RPLY_SAC ON IMRD_F.EN_MRD06P2(RPLY_TBL_DCOD_3845, SAC_8990) TABLESPACE IMRD_FTBL;
