DROP TABLE IMRD_F.EN_MRD_H6 CASCADE CONSTRAINTS;

--criando a tabela com a PK ID
CREATE TABLE IMRD_F.EN_MRD_H6
(
    NM_CD_2303          VARCHAR2(2048 BYTE),
    APPLB_KEY_CD_0103   VARCHAR2(2048 BYTE),
    DT_ESTB_CANC_0308   VARCHAR2(2048 BYTE),
    STA_CD_NM_FSC_0320  VARCHAR2(2048 BYTE),
    SHRT_NM_2301        VARCHAR2(2048 BYTE),
    NM_PREFIX_2516      VARCHAR2(2048 BYTE),
    NM_ROOT_RMDR_2517   VARCHAR2(2048 BYTE),
    FSC_COND_CD_2607    VARCHAR2(2048 BYTE),
    ITM_NM_TYP_CD_3308  VARCHAR2(2048 BYTE),
    FIIG_4065           VARCHAR2(2048 BYTE),
    CONCEPT_NBR_4488    VARCHAR2(2048 BYTE),
    ITM_NM_DEF_5015     VARCHAR2(2048 BYTE),
    NM_FLD_2304         VARCHAR2(2048 BYTE), --concat SHRT_NM_2301 || NM_ROOT_RMDR_2517
    ID                  NUMBER(19),
    PRIMARY KEY(ID)
)
TABLESPACE IMRD_FTBL
RESULT_CACHE (MODE DEFAULT)
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
            FLASH_CACHE      DEFAULT
            CELL_FLASH_CACHE DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;

--criando os sinonimos publicos
CREATE OR REPLACE PUBLIC SYNONYM EN_MRD_H6 FOR IMRD_F.EN_MRD_H6;

--criando indices
CREATE UNIQUE INDEX IDX_EN_MRD_H6_INC ON IMRD_F.EN_MRD_H6(NM_CD_2303) TABLESPACE IMRD_FTBL; 